import {apiUsers} from "../API/api";

const FOLLOW = 'FOLLOW'
const UNFOLLOW = 'UNFOLLOW'
const SET_USERS = 'SET_USERS'
const SET_CURRENT_PAGE = 'SET_CURRENT_PAGE'
const SET_TOTAL_USERS_COUNT = 'SET_TOTAL_USERS_COUNT'
const TOGGLES_IS_FETCHING = 'TOGGLES_IS_FETCHING'
const TOGGLE_IS_FOLLOWING_PROGRESS = 'TOGGLE_IS_FOLLOWING_PROGRESS'
//в эту переменную мы заносим данные что бы смогла отресоваться страница и передаем редюсеру ниже
let initialState = {
    UsersPage: [],
    pageSize: 4,
    totalUsersCount: 0,
    currentPage: 1,
    isFetching: false,
    foloowingProgress: []       //По умолчанию масив имеет псевдо истину True
};


const UsersReducer = (state = initialState, action) => {
    switch (action.type) {
        case FOLLOW:
            return {
                ...state,
                UsersPage: state.UsersPage.map(u => {
                    if (u.id === action.usersId) {
                        return { ...u, followed: true }
                    }
                    return u;
                })
            }
        case UNFOLLOW:
            return {
                ...state,
                UsersPage: state.UsersPage.map(copyUsers => {
                    if (copyUsers.id === action.usersId) {
                        return { ...copyUsers, followed: false }
                    }
                    return copyUsers;
                })
            }
        case SET_USERS:
            // return { ...state, UsersPage: [...state.UsersPage, ...action.users] }  //Добовляем в уже существующий массив юзеров новых
            //в конец массива, будут добовляться ниже а что бы Юзеры менялись на других и не добовлялись в низ нужно полносью перезаписать массив
            return { ...state, UsersPage: action.users }
        case SET_CURRENT_PAGE:
            return { ...state, currentPage: action.currentPage }
        case SET_TOTAL_USERS_COUNT:
            return { ...state, totalUsersCount: action.count }
        case TOGGLES_IS_FETCHING:
            return { ...state, isFetching: action.fetching }
        case TOGGLE_IS_FOLLOWING_PROGRESS:
            return {
                ...state,
                foloowingProgress: action.progress
                    ? [...state.foloowingProgress, action.userId]
                    : [...state.foloowingProgress.filter(id => id != action.userId)]
            }
        default:
            return state

    }
}
export const follow = (usersId) => ({ type: FOLLOW, usersId })
export const unfollow = (usersId) => ({ type: UNFOLLOW, usersId })
export const setUsers = (users) => ({ type: SET_USERS, users })   // С помощью этого экшана мы будем брать данный с БД
export const setCurrentPage = (currentPage) => ({ type: SET_CURRENT_PAGE, currentPage })
export const setTotalCountUsers = (count) => ({ type: SET_TOTAL_USERS_COUNT, count })
export const toggleFetching = (fetching) => ({ type: TOGGLES_IS_FETCHING, fetching })
export const toggleFollowingProgress = (progress, userId) => ({ type: TOGGLE_IS_FOLLOWING_PROGRESS, progress, userId })


export const getUsersThunkCreator = (currentPage, pageSize) => {
    return async (dispatch) => {
        dispatch(toggleFetching(true));
       let response = await apiUsers.getUsersApi(currentPage, pageSize)
            dispatch(setCurrentPage(currentPage))
            dispatch(toggleFetching(false))
            dispatch(setUsers(response.items))
            dispatch(setTotalCountUsers(response.totalCount))
    }
}
export const UnfoloowThunkCreator = (id) => {
    return async (dispatch) => {
        dispatch(toggleFollowingProgress(true, id))
      let response = await  apiUsers.UnfollowAPI(id)
            if (response.data.resultCode === 0) {
                dispatch(unfollow(id))
            }
            dispatch(toggleFollowingProgress(false, id))
    }
}
export const FollowThunkCreator = (id) => {
    return async (dispatch) => {
        dispatch(toggleFollowingProgress(true, id))
       let response = await apiUsers.FollowAPI(id)
            if (response.data.resultCode === 0) {
                dispatch(follow(id))
            }
            dispatch(toggleFollowingProgress(false, id))
    }
}
export default UsersReducer
