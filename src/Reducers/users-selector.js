export const getUsersSelect = (state) => {
   return state.UsersPageR.UsersPage
}

export const getPageSizeSelect = (state) => {
    return state.UsersPageR.pageSize
}
export const getTotalCountSelect = (state) => {
    return state.UsersPageR.totalUsersCount
}
export const getCurrentPageSelect = (state) => {
    return state.UsersPageR.currentPage
}
export const getFechingSelect = (state) => {
    return state.UsersPageR.isFetching
}
export const getFollowingProgressSelect = (state) => {
    return state.UsersPageR.foloowingProgress
}