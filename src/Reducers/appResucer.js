import {authMeThunckCreator} from "./aut-log-reducer";


const INITIALIZED_SUCCSESS = 'SET_INITIALIZED'



let initialState = {
    initialized: false,

};


const AppReducer = (state = initialState, action) => {
    switch (action.type) {
        case INITIALIZED_SUCCSESS:
            return {
                ...state,
                initialized: true
            }
        default:
            return state

    }
}

export const InitializedSuccsess = () => ({ type: INITIALIZED_SUCCSESS })


export const initializeApp = () => (dispatch) => {
    let promise = dispatch(authMeThunckCreator());    //Промис это данные которые приходят с AuthReducer когда мы вызваем там эту санку
    // и из-за того что мы там написали return сюда приходят эти данные, и только после того как мы зайдем в акк выполниться промис.then
    promise.then(()=>{
        dispatch(InitializedSuccsess());
    })
}


export default AppReducer