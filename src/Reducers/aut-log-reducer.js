import {AuthLogin} from "../API/api";

const SET_USERS_DATA = 'SET_USERS_DATA'
const ERRORS_LOGIN = 'ERRORS_LOGIN'

let initialState = {
    id: null,
    email: null,
    login: null,
    isAuth: false,
    errorLogin:true
};


const AuthLoginReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_USERS_DATA:
            return {
                ...state,
                ...action.data      //этот экшен полность перезапишет стате его данные которые прийдут с экшена
                // isAuth:true          //Если данные пришли то это значение будет True
            }
        case ERRORS_LOGIN:
            return {
                ...state,errorLogin:false
            }

        default:
            return state

    }
}
export const setUserData = (id,email,login,isAuth,errorLogin) => ({ type: SET_USERS_DATA, data:{id,email,login,isAuth,errorLogin } })
export const errorLogi = () => ({type:ERRORS_LOGIN})
export const authMeThunckCreator = () => (dispatch)=>{
    return AuthLogin.AuthME().then(response => {
        if (response.data.resultCode === 0){
            let {id,email,login} = response.data.data      //Берем значения которые к нам приходят от сервера можно посмотреть дебагером
            dispatch(setUserData(id,email,login,true,true));
        }
    });
}
export const LoginThunk = (email, password) => (dispatch)=>{
    AuthLogin.LoginAPI(email, password).then(response =>{
        if (response.data.resultCode === 0){
            dispatch(authMeThunckCreator())
        } else {
            dispatch(errorLogi())
        }
    })
}

export const LogOutThunk = () => (dispatch)=>{
    AuthLogin.LogOutAPI().then(response =>{
        if (response.data.resultCode === 0){
            dispatch(setUserData(null,null,null,false,true));
        }
    })
}


export default AuthLoginReducer
