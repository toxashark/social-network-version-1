import {ProfileAPI as APiDLL, ProfileAPI} from "../API/api";

const ADD_POST = 'ADD-POST'
const SET_PROFILE_USERS = 'SET_PROFILE_USERS'
const SET_STATUS = 'SET_STATUS'
const SAVE_PHOTO = 'SAVE_PHOTO'

//в эту переменную мы заносим данные что бы смогла отресоваться страница и передаем редюсеру ниже
let initialState = {
    PostMassage: [
        { id: 1, text: 'Мое Описание об Картинке, Дорогой дневник', likecount: '5' },
        { id: 2, text: 'Мое Описание об Картинке, Дорогой дневник ', likecount: '44' },

    ],

    profile: null,
    status: ''
};


const ProfileReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_POST: {
            let newPost = {
                id: 6,
                text: action.postText,
                likecount: 'Like: 0'
            }
            return {
                ...state,
                PostMassage: [ newPost,...state.PostMassage],
            }
        }
        case SET_PROFILE_USERS: {
            return { ...state, profile: action.profile }
        }
        case SET_STATUS: {
            return { ...state, status: action.status }
        }
        case SAVE_PHOTO:{
            return {...state,profile:{...state.profile,photos: action.photo}}
        }
        default:
            return state
    }
}
export const setProfileUser = (profile) => ({ type: SET_PROFILE_USERS, profile })
export const setStatus = (status) => ({ type: SET_STATUS, status })
export const actionCreatedAddPost = (postText) => ({ type: ADD_POST, postText })
export const savePhotoSuccsess = (photo) => ({type:SAVE_PHOTO, photo})
export default ProfileReducer

export const getProfileThunkCreator = (userid) => (dispatch) => {
    return APiDLL.GetUsersIDAPI(userid).then(response => {
        dispatch(setProfileUser(response.data))
    });
}

export const getStatus = (userid) => (dispatch) => {
    return ProfileAPI.getStatus(userid).then(response => {
        dispatch(setStatus(response.data))
    })
}
export const savePhoto = (file) => async (dispatch) => {
    let response = await  ProfileAPI.savePhoto(file)
        dispatch(savePhotoSuccsess(response.data.data.photos))
}

export const updateStatus = (status) => (dispatch) => {
    return ProfileAPI.upDateStatus(status).then(response => {
        if (response.data.resultCode === 0) {
            dispatch(setStatus(status))
        }
    })
}