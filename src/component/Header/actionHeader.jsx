import React from 'react'
import {connect} from 'react-redux';
import {LogOutThunk} from "../../Reducers/aut-log-reducer";
import Header from "./Header";
import HeaderPictures from "./HeaderPictures";
import {getProfileThunkCreator} from "../../Reducers/contentReducer";
import {withRouter} from "react-router";


class HeaderConrainer extends React.Component {
    componentDidMount() {
        let userid = this.props.match.params.useid
        if (!userid) {
            userid = this.props.userId;
        }

        this.props.getProfileThunkCreator(userid);

    }

    render() {
        if (this.props.profile === null) return <h2>Loading</h2>
        return (
            <div>
                <Header {...this.props} profile={this.props.profile}/>
                <HeaderPictures profile={this.props.profile}/>
            </div>


        );
    }
}


const mapStatetoProps = (state) => ({
    isAuth: state.authLogi.isAuth,       //Берем данные с редюсера
    login: state.authLogi.login,
    userId:state.authLogi.id,
    profile: state.Content.profile
});

const withRouteContainer = withRouter(HeaderConrainer)
export default connect(mapStatetoProps, {LogOutThunk,getProfileThunkCreator})(withRouteContainer)