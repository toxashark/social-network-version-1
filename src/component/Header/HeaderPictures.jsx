import React from 'react'
import photoUser from "../Users/user.png";




const HeaderPictures = (props) =>{
    return (
        <div>

            <navbar>

                    <img className='mainAvatar' src={props.profile.photos.large != null ? props.profile.photos.large : photoUser} alt=""/>

                <img className='header-image' src="image/header-pictures.jpg" alt=""/>
            </navbar>
        </div>
    )
}

export default HeaderPictures