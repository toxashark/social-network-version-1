import React from 'react'
import {NavLink} from "react-router-dom";





const Header = (props) => {

    return (
        <div>
            <header>
                <img className='imageIcon' src="image/icon.png" alt=""/>
                <div>
                    <ul className='style-menu'>
                        <li> <NavLink to="/profile">Profile</NavLink></li>
                        <li><NavLink to="/users">Users</NavLink></li>
                        <li><NavLink to="/page">Page</NavLink></li>
                        <li><a href="#">Setting</a></li>
                    </ul>
                </div>
                <div className='Loginstyle'>
                    {props.isAuth
                        ? <div>{props.login } - <button onClick = {props.LogOutThunk}>Log out</button> </div>
                        :  <NavLink to = {'/login'} >Login</NavLink>}
                    {/* если isAuth будет ТРУ тогда покажеться Логин что мы подключились если нет перекинет на страницу ЛОгин */}

                </div>
            </header>

        </div>
    )
}


export default Header