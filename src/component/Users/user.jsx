import React from 'react'
import {NavLink} from 'react-router-dom'

import photoUser from './user.png'




const User = (props) => {
    return (
        <div className='userscontent'>
            {props.users.map(users => <div className='usersStyle' key={users.id}>
                <span>
                    <div>
                        <NavLink to={'/profile/' + users.id}>
                            <img src={users.photos.small != null ? users.photos.small : photoUser}  />
                        </NavLink>
                    </div>
                    <div>

                        {users.followed
                            ? <button disabled={props.foloowingProgress.some(id => id === users.id)} onClick={() => {
                                props.UnfoloowThunkCreator(users.id);

                            }}>UnFolllo</button>
                            : <button disabled={props.foloowingProgress.some(id => id === users.id)} onClick={() => {
                                props.FollowThunkCreator(users.id);

                            }}>Follow</button>}
                    </div>
                </span>
                <div>
                    <span>
                        <div>{users.name}</div>
                        <div>{users.status}</div>

                    </span>

                </div>
            </div>)
            }
        </div>
    )
}
export default User


