import React from 'react'
import { connect } from "react-redux";
import Users from './users'
// import { Redirect } from 'react-router';
// import {withAuthRedirect} from '../../HOK/WithAuthRedirectHok'
import { compose } from 'redux';

import {FollowThunkCreator, getUsersThunkCreator, UnfoloowThunkCreator} from "../../Reducers/users-page-reducer";
import {
    getCurrentPageSelect, getFechingSelect, getFollowingProgressSelect,
    getPageSizeSelect,
    getTotalCountSelect,
    getUsersSelect
} from "../../Reducers/users-selector";
import Preloader from "../../common/preloader";
import {withAuthRedirect} from "../../common/HOK/WithAuthRedirectHok";


class UsersComponent extends React.Component {

    componentDidMount() {

        this.props.getUsersThunkCreator(this.props.currentPage, this.props.pageSize);
    }
    //callback функция которая меняет страницы с пользователями 
    onPageChanged = (pageNumber) => {
         this.props.getUsersThunkCreator(pageNumber, this.props.pageSize);

    }

    render() {
        if (this.props.totalUsersCount === 0) return <h2>Loading</h2>

        return <>
            {/* тернарное условия на появление или уберание картинки Preloader */}
            {this.props.isFetching ? <Preloader /> : null}

            <Users totalUsersCount={this.props.totalUsersCount}
                pageSize={this.props.pageSize}
                currentPage={this.props.currentPage}
                onPageChanged={this.onPageChanged}
                users={this.props.users}
                toggleFollowingProgress={this.props.toggleFollowingProgress}
                foloowingProgress={this.props.foloowingProgress}
                UnfoloowThunkCreator ={this.props.UnfoloowThunkCreator}
                FollowThunkCreator = {this.props.FollowThunkCreator}

            />
        </>

    }
}

let mapStateToProps = (state) => {
    return {
        users: getUsersSelect(state),
        pageSize: getPageSizeSelect(state),
        totalUsersCount: getTotalCountSelect(state),
        currentPage: getCurrentPageSelect(state),
        isFetching: getFechingSelect(state),
        foloowingProgress: getFollowingProgressSelect(state),
       
    }
}

export default compose(
    connect(mapStateToProps, {getUsersThunkCreator,UnfoloowThunkCreator,FollowThunkCreator,}),
    withAuthRedirect
)(UsersComponent)

