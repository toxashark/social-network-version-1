import User from './user'
import Pagination from "../../common/pagination/pagination";

const Users = (props) => {
    
    
    
    return (<div>

        <Pagination currentPage={props.currentPage}
            onPageChanged={props.onPageChanged}
            totalUsersCount={props.totalUsersCount} pageSize={props.pageSize} />
            
        <User users={props.users} foloowingProgress={props.foloowingProgress}
            FollowThunkCreator={props.FollowThunkCreator}
             totalUsersCount={props.totalUsersCount} UnfoloowThunkCreator ={props.UnfoloowThunkCreator} />

    </div>
    )
}


export default Users;


