import React from 'react';
import {connect} from "react-redux";
import PagesUser from "./pagesUsers";
import {compose} from "redux";
import {withRouter} from "react-router";
import {withAuthRedirect} from "../../common/HOK/WithAuthRedirectHok";
import {getStatus, getProfileThunkCreator, savePhoto, updateStatus} from "../../Reducers/contentReducer";







class ActionPagesUsers extends React.Component {
componentDidMount() {
    let userid = this.props.match.params.useid
    if (!userid) {
        userid = this.props.userId;
    }

    this.props.getProfileThunkCreator(userid);
    this.props.getStatus(userid)
}
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(this.props.match.params.useid !== prevProps.match.params.useid) {
            let userid = this.props.match.params.useid
            if (!userid) {
                userid = this.props.userId;
            }

            this.props.getProfileThunkCreator(userid);
            this.props.getStatus(userid)
        }
    }


    render() {
        if (this.props.profile === null) return <h2>Loading</h2>
        return (
            <div>
                <PagesUser {...this.props} savePhoto={this.props.savePhoto}
                           isOwner = {!this.props.match.params.useid}
                           updateStatus = {this.props.updateStatus}
                           profile={this.props.profile}
                           status= {this.props.status}
                           nameUsers={this.props.nameUsers}/>
            </div>
        );
    }
}
let mapStateToProps = (state) => {
    return {
        profile:state.Content.profile,
        status:state.Content.status,
        userId:state.authLogi.id,
        nameUsers:state.UsersPageR.UsersPage
    }
}
export default compose(
    connect(mapStateToProps, { getProfileThunkCreator, getStatus, updateStatus,savePhoto }),
    withRouter,
    withAuthRedirect
)(ActionPagesUsers)
