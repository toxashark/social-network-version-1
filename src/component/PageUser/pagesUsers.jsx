import React from 'react'
import ProfileStatusWithHooks from "./ProfileStatusHooks";
import photoUser from "../Users/user.png";


const PagesUser = (props) => {
    const mainPhoto = (e) => {
        if(e.target.files.length){
            props.savePhoto(e.target.files[0])
        }
    }
    return (
        <div className='StylePages'>
            <img src={props.profile.photos.large != null ? props.profile.photos.large : photoUser} alt=""/>
            {props.isOwner && <input type={"file"} onChange={mainPhoto} />}
            <div>
                <ProfileStatusWithHooks status={props.status} updateStatus={props.updateStatus}/>
            </div>
            <div>
                {props.nameUsers.name}
            </div>
        </div>
    )
}


export default PagesUser