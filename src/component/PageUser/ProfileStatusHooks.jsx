import { useState, useEffect } from "react"
import React from 'react'




const ProfileStatusWithHooks = (props) => {
    let [status, setStatus] = useState(props.status)
    let [editMode, setEditMode] = useState(false)   //Первым значением в масиве у нас будет то что мы передадим с помощью хука в скобках
    // а второе значение эта функция в которую мы передаем параметр и она изменит 1 параметр
    useEffect(() => {           //Запускаеться в самом конце когда полностью страница уже будет отрисована
        setStatus(props.status);
    }, [props.status])

    const activeEditMode = () => {
        setEditMode(true)
    }
    const onStatusChange = (event) => {
        setStatus(event.currentTarget.value)
    }
    const deactiveEditMode = () => {
        setEditMode(false)
        props.updateStatus(status)
    }
    return (
        <div>
            {!editMode &&
                <div>
                    <span onDoubleClick={activeEditMode}>{props.status || 'New Status'}</span>
                </div>
            }
            {editMode &&
                <div>
                    <input onChange={onStatusChange} value={status} onBlur={deactiveEditMode} autoFocus={true}
                        type="text" />
                </div>
            }
        </div>
    )
}



export default ProfileStatusWithHooks