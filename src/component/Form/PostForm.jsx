import React from 'react'
import * as Yup from "yup";
import {ErrorMessage, Field, Form, Formik} from 'formik';
import {actionCreatedAddPost} from "../../Reducers/contentReducer";



const ContentForm = (props) => {

    const initialValues = {
        post: ''
    }
    const onSubmit = (values,onSubmitProps) => {
        props.actionCreatedAddPost(values.post)
        onSubmitProps.resetForm()
        // console.log(values)
    }
    const validationSchema = Yup.object({
        post: Yup.string().required('Required'),

    })
    return(
          <Formik onSubmit={onSubmit} initialValues={initialValues}
                  validateOnBlur={false}  validationSchema={validationSchema}>
              {formik => {return(
                <Form>
                    <div>
                        <div>
                            <label htmlFor="MyPost"></label>
                            <Field as='textarea'
                                   id='post'
                                   placeholder ='write something....'
                                   name='post' />
                            <ErrorMessage name='post'/>
                        </div>
                        <button className='submitForm' type='submit'>Submit</button>
                    </div>
                </Form>
              )}}
          </Formik>
    )
}
export default ContentForm

