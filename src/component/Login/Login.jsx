import React from 'react'
import {Formik, Form, Field, ErrorMessage,} from 'formik'
import * as Yup from 'yup'
import style from './styleForm.module.css'
import {connect} from 'react-redux'
import {Redirect} from 'react-router'
import {LoginThunk} from "../../Reducers/aut-log-reducer";


const Login = (props) => {
    const initialValues = {
        password: '',
        email: '',
    }
    const onSubmit = (values, onSubmitProps) => {              //Так можем посмотреть какие данные нам приходят, после нажатия отправить
        // console.log(values)
        props.LoginThunk(values.email, values.password)
        onSubmitProps.resetForm()
    }
    const validationSchema = Yup.object({
        email: Yup.string().email('Invalid email format').required('Required'),
        password: Yup.string().required('Pleas again'),
    })

    const StyleRequired = (props) => {
        return (
            <div className={style.requredStyle}>
                {props.children}
            </div>
        )
    }
    const redircetFalls = () => {
        window.location.reload();      //Перезагружает страницу
    }
    const errorsForm = (props) => {
        return (
            <div className='grid-login'>
                <LoginForm/>
                <div className='ErrorsText'>
                    Вы вели неправильной логин или пороль
                </div>
            </div>
        )
    }


    const LoginForm = (props) => {
        // console.log('Значение записываеться', formik.errors);
        return (
            <Formik
                initialValues={initialValues}
                onSubmit={onSubmit}
                validationSchema={validationSchema}>
                {/* onSumbit  Реагирует на нажатие и отправляет форму */}
                {formik => {
                    // console.log('Formik props', formik)
                    return (

                        <Form>
                            <div>
                                {/* форма ошибки */}
                                <div>
                                    <label className={style.label} htmlFor="email">Email</label>
                                    <Field className={style.inputStyle}
                                           type="email"
                                           id="email"
                                           name="email"
                                           placeholder = 'example@gmail.com'
                                    />

                                    <ErrorMessage name='email' component={StyleRequired}/>
                                </div>
                                <div>
                                    <label className={style.label} htmlFor="password">Password</label>
                                    <Field className={style.inputStyle}
                                           type="password"
                                           id="password"
                                           name="password"
                                           placeholder = '*******'
                                    />
                                    <ErrorMessage name='password' component={StyleRequired}/>

                                </div>

                                <button type="submit">Sumbit</button>
                            </div>

                        </Form>
                    )
                }}
            </Formik>
        )
    }
    //Проверка на появление ошибки Логирования
    if (!props.errorLogin) {
        return errorsForm()
    }
    //Когда мы автаризуемся перекинет на Профиль
    if (props.isAuth) {
        return <Redirect to={'/profile'}/>
    }
    return (
        <div className='grid-login'>
            <h1 className='Formtext'>Login</h1>
            <LoginForm/>
        </div>
    )
}
const mapStateToProps = (state) => ({
    isAuth: state.authLogi.isAuth,
    errorLogin: state.authLogi.errorLogin
})

export default connect(mapStateToProps, {LoginThunk})(Login)
