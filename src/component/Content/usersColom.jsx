import React from 'react'
import {NavLink} from "react-router-dom";
import photoUser from "../Users/user.png";




class UsersColom extends React.Component {
    render() {
        return (
            <div>
                <div className='styleUsersColom'>
                    <div className='titleUsersColoms'>
                        Users
                    </div>
                    {this.props.users.map(users=><div className='colomsUsersContainer' key={users.id}>
                        <div>
                            <NavLink to={'/profile/' + users.id}>
                                <img src={users.photos.small != null ? users.photos.small : photoUser}/>
                            </NavLink>
                        </div>
                        <div>
                            {users.name}
                        </div>
                        <div>
                            {users.status}
                        </div>
                    </div>)}
                    <form className='styleButtonUsersColoms' action="/users">
                        <button type="submit">More Users</button>
                    </form>
                </div>

            </div>
        );
    }
}

export default UsersColom;