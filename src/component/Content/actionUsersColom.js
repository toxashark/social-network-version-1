import React from 'react'
import {connect} from "react-redux";
import UsersColom from "./usersColom";
import {getUsersThunkCreator} from "../../Reducers/users-page-reducer";





class ContainerUsersColoms extends React.Component {
    componentDidMount() {
        this.props.getUsersThunkCreator(this.props.currentPage, this.props.pageSize)
    }

    render() {
        if (this.props.totalUsersCount === 0) return <h2 className='loadigUersColoms'>Loading...</h2>
        return (
            <div>
                <UsersColom users={this.props.users}/>
            </div>
        );
    }
}

let mapStateToProps = (state) => {
    return {
        users:state.UsersPageR.UsersPage,
        currentPage:state.UsersPageR.currentPage,
        pageSize:state.UsersPageR.pageSize,
        totalUsersCount:state.UsersPageR.totalUsersCount,

    }
}


export default connect(mapStateToProps,{getUsersThunkCreator})(ContainerUsersColoms)