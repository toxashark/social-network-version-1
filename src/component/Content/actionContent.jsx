import React from "react";
import Content from "./content";
import {connect} from "react-redux";
import {actionCreatedAddPost, getProfileThunkCreator} from "../../Reducers/contentReducer";
import {compose} from "redux";
import {withAuthRedirect} from "../../common/HOK/WithAuthRedirectHok";
import {withRouter} from "react-router";


class ContentContainer extends React.Component{
    componentDidMount() {
        let userid = this.props.match.params.useid
        if (!userid) {
            userid = this.props.userId;
        }

        this.props.getProfileThunkCreator(userid);
    }

    render() {
        if (this.props.myPhoto === null) return <h2 className='loadigUersColoms'>Loading</h2>
        return (

            <Content myPhoto={this.props.myPhoto} login={this.props.login} PostMassage={this.props.PostMassage}
                     actionCreatedAddPost={this.props.actionCreatedAddPost}/>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        PostMassage: state.Content.PostMassage,
        myPhoto: state.Content.profile,
        userId:state.authLogi.id,
        login:state.authLogi.login
    }
}


// export default connect(mapStateToProps, {actionCreatedAddPost})(ContentContainer)
export default compose(connect(mapStateToProps, {actionCreatedAddPost, getProfileThunkCreator}),
    withRouter,
    withAuthRedirect)
(ContentContainer)