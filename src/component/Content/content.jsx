import React from 'react'
import ContentForm from "../Form/PostForm";
import Posts from "./post";
import ContainerUsersColoms from "./actionUsersColom";


const Content = (props) => {

    let PostCreate = props.PostMassage.map(post => <Posts login={props.login} myPhoto={props.myPhoto} massage={post.text}
                                                          likecount={post.likecount}/>)
    return (
        <div>

            <div className="content">
                <div className="socia">
                    <div className="textSocial">
                        Social
                    </div>
                    <div className="facebook">
                        <div className="textFacebook">
                            Facebook
                        </div>

                        <img src="image/facebook.png" alt=""/>
                    </div>
                    <div className="twitter">
                        <div className='textTwitter'>
                            Twitter
                        </div>
                        <img src="image/twiter.png" alt=""/>
                    </div>
                    <div className="google">
                        <div className='textGoogle'>
                            Google
                        </div>
                        <img src="image/google.png  " alt=""/>
                    </div>
                    <div>
                        <ContainerUsersColoms/>
                    </div>
                </div>
                <div className="post">
                    <ContentForm actionCreatedAddPost={props.actionCreatedAddPost}/>
                    <div className='PostUsers'>
                        {PostCreate}
                    </div>
                </div>

                <div className="advertising">
                    <img src="image/reklama.jpeg" alt=""/>
                </div>
            </div>

        </div>
    )
}


export default Content