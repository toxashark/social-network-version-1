import React from "react";
import photoUser from "../Users/user.png";


const Posts = (props) => {

    return (
        <div className='postContent'>
            <div>
                <span>
                <img className='stylePhotoPost'
                     src={props.myPhoto.photos.large !== null ? props.myPhoto.photos.large : photoUser} alt=""/>
                     <span className='styleNameUsersPost'>
                    {props.login}
                    </span>
                    <span className='styledata'>
                        Published: 11:00 AM 22.03.2021
                    </span>
                     </span>
                <div className='textPost'>
                    {props.massage}
                </div>
            </div>
            <div>
                <img className='picturesPost' src="image/post.jpg" alt=""/>
            </div>
        </div>
    )
}

export default Posts