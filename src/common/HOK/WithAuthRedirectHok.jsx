import React from 'react'
import { connect } from 'react-redux';
import { Redirect } from 'react-router';





let mapStateToprops = (state) => {
    return {
        isAuth:state.authLogi.isAuth
    }
}

export const withAuthRedirect = (Component) => {
    class RedirectComponent extends React.Component {
        render (){
            if (this.props.isAuth === false) return <Redirect to = '/login'/>;
            return <Component {...this.props} />
        }
    }
    let ConnetctAuthRedirectComponent = connect(mapStateToprops)(RedirectComponent)
    return ConnetctAuthRedirectComponent;
}