import * as React from "react";
import ContentContainer from "./component/Content/actionContent";
import {BrowserRouter, Route} from "react-router-dom";
import UsersContainer from './component/Users/users-container'
import LoginPage from "./component/Login/Login";
import HeaderConrainer from "./component/Header/actionHeader";
import {initializeApp} from "./Reducers/appResucer";
import {compose} from "redux";
import {connect} from "react-redux";
import Preloader from "./common/preloader";
import ActionPagesUsers from "./component/PageUser/actionPagesUsers";
import photoUser from "./component/Users/user.png";



class App extends React.Component {
    componentDidMount() {
        this.props.initializeApp();
    }

    render() {
        if (!this.props.initialzedBolean) {
            return <Preloader/>
        }
        return (
            <BrowserRouter>
                <div className="grid-container">
                    <div>
                        <HeaderConrainer/>
                    </div>
                    <main>
                        <Route path='/page' render={() => <ContentContainer/>}/>
                        <Route path='/users' render={() => <UsersContainer/>}/>
                        <Route path='/profile/:useid?' render={() => <ActionPagesUsers/>}/>
                        <Route path='/login' render={() => <LoginPage/>}/>
                    </main>
                </div>
            </BrowserRouter>
        );
    }
}

const mapStateTiProps = (state) => ({
    initialzedBolean: state.authApp.initialized,

})
export default compose(
    connect(mapStateTiProps, {initializeApp}))(App)