import * as axios from 'axios'




const instance = axios.create({
    withCredentials: true,
    headers: { "API-KEY": "f1e9fa35-f7c2-4c4c-a85b-e0ffa2d98781" },
    baseURL: 'https://social-network.samuraijs.com/api/1.0/',

})

export const apiUsers =  {
    getUsersApi(currentPage, pageSize) {
        return instance.get(`users?page=${currentPage}&count=${pageSize}`).then(response => { return response.data })
    },
    UnfollowAPI(id){
        return instance.delete(`follow/${id}`)
    },
    FollowAPI (id) {
        return instance.post(`follow/${id}`)
    },
}
export const AuthLogin = {
    AuthME() {
        return instance.get(`auth/me`)
    },
    LoginAPI (email, password) {
        return instance.post(`auth/login`,{email, password})
    },
    LogOutAPI () {
        return instance.delete(`auth/login`)
    },
}
export const ProfileAPI = {
    GetUsersIDAPI (userid) {
        return instance.get(`profile/` + userid)
    },
    getStatus (userid) {
        return instance.get(`profile/status/` + userid)
    },
    upDateStatus (status) {
        return instance.put(`profile/status/`,{status: status})   //Создваем переменную статус и в нее ложим action который к нам прийдет и
        // Этот Акшен будет отправлен на сервер(какойто статус (текст))
    },
    savePhoto(photosFile) {
        const formData = new FormData();
        formData.append('image',photosFile);
        return instance.put(`profile/photo`,formData,{
            headers : {
                'Content-Type':'multipart/form-data'
            }
        })
    }
}