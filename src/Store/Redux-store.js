import {createStore, combineReducers, applyMiddleware, compose} from 'redux'
import thunkMiddleware from 'redux-thunk'
import ProfileReducer from "../Reducers/contentReducer";
import UsersReducer from "../Reducers/users-page-reducer";
import AuthLoginReducer from "../Reducers/aut-log-reducer";
import AppReducer from "../Reducers/appResucer";



// Функция Redux которая объеденяет все редюсеры, каждый редюсер это объект
let reducersGroup = combineReducers({
Content:ProfileReducer,
    UsersPageR: UsersReducer,
    authLogi:AuthLoginReducer,
    authApp:AppReducer,
});
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducersGroup, composeEnhancers(applyMiddleware(thunkMiddleware)));


export default store